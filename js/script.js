
(function(window) {

    var document = window['document'];

    var $ = window.jQuery;

    var circleCenter = (800 - 268) / 2;

    var maxWidth = parseInt($('.teaser').css('max-width'), 10);
    var letterSpacing = parseInt($('.claim').css('letter-spacing'), 10);

    var DOM = {
        staySmart: $('.imageBox')[0],
        imgTop: $('.topImg'),
        imgBottom: $('.bottomImg'),
        claim: $('.claim')[0],
        c1: $('#c1')[0],
        c2: $('#c2')[0],
        c3: $('#c3')[0],
        t1: $('#t1')[0],
        t2: $('#t2')[0],
        top: $(".top")[0],
        info: $('#infografix')[0]
    };

    var domChange = {
        claimSize: 110,
        claimLeft: 30,
        claimTop: 30,
        staySmartHeight: 480,
        imgOffset: 0,
        c1: circleCenter,
        c2: circleCenter,
        c3: circleCenter,
        t1: 0,
        t2: 0,
        top: 0,
    };

    var domElements = {
        claimSize: [-1, DOM.claim, "fontSize", "px"],
        //claimLeft: [-1, DOM.claim, "left", "px"],
        //claimTop: [-1, DOM.claim, "top", "px"],
        staySmartHeight: [-1, DOM.staySmart, "height", "px"],
        imgOffset: [-1, DOM.staySmart.childNodes[1], "marginTop", "px"],
        c1: [-1, DOM.c1, "left", "px"],
        c2: [-1, DOM.c2, "left", "px"],
        c3: [-1, DOM.c3, "left", "px"],
        t1: [-1, DOM.t1, "opacity", ""],
        t2: [-1, DOM.t2, "opacity", ""],
        top: [-1, DOM.top, "opacity", ""],
    };

    var $anim = {};

    DOM.top.onclick = function(ev) {
        ev.preventDefault();

        var start = NOW;

        var curPX = $(window).scrollTop();
        var speed = 700; // 300px / sec

        $anim['top'] = function(t) {

            var cur = (t - start) / (curPX / speed * 1000);

            if (cur >= 1) {
                cur = 1;
                delete $anim['top'];
            }

            var k = cur;
            cur = --k * k * k + 1;

            window.scrollTo(0, curPX - Math.floor(cur * curPX));

        };
    };


    var NOW;
    (function(domChange, domElements) {

        var fn;
        window.requestAnimationFrame(fn = function(t) {

            NOW = t;

            for (var i in domElements) {

                var tmp = domElements[i];

                if (domChange[i] !== tmp[0]) {
                    domElements[i][0] = domChange[i];
                    tmp[1].style[tmp[2]] = domChange[i] + tmp[3];
                }
            }

            for (var i in $anim) {
                $anim[i](t);
            }

            window.requestAnimationFrame(fn);
        });

    })(domChange, domElements);

    var fadeTime = 2000;
    var state = [0,0];
    var prevFade = 0;
    $anim['cross'] = function(t) {

        var cur = (t % fadeTime) / fadeTime;
        
        function run(cur, i) {

            if (state[i] === 0) {

                if (prevFade > cur) {
                    state[i] = 1;
                    DOM.imgBottom[i].style.zIndex = 1000;
                    DOM.imgTop[i].style.zIndex = 900;
                    DOM.imgTop[i].style.opacity = 1;
                } else {
                    DOM.imgTop[i].style.opacity = 1 - cur;
                }

            } else if (state[i] === 1) {

                if (prevFade > cur) {
                    state[i] = 2;
                }

            } else if (state[i] === 3) {

                if (prevFade > cur) {
                    state[i] = 0;
                }

            } else if (state[i] === 2) {

                if (prevFade > cur) {
                    state[i] = 3;
                    DOM.imgTop[i].style.zIndex = 1000;
                    DOM.imgBottom[i].style.zIndex = 900;
                    DOM.imgBottom[i].style.opacity = 1;
                } else {
                    DOM.imgBottom[i].style.opacity = 1 - cur;
                }
            }
        }
        
        run(cur, 0);
        run(cur, 1);

        prevFade = cur;
    };

    function positionStaySmart() {

        var w = Math.min(maxWidth, window.innerWidth);

        var ratio = 1389 / 658;

        domChange.claimSize = Math.min(Math.ceil(7.54718 * Math.exp(0.00191459 * w)), 110);
        domChange.staySmartHeight = Math.floor(w / ratio) - 19;
        domChange.claimLeft = Math.ceil(608.74 - 0.407541 * w) + letterSpacing;
        domChange.claimTop = Math.ceil(0.199151 * w - 16.0878);
    }
	
    function positionCircles() {


    }

    var sections = [
        document.getElementsByTagName("header")[0],
        document.getElementsByClassName("teaser")[0],
        document.getElementsByClassName("usp")[0],
        document.getElementsByClassName("info")[0],
        document.getElementsByClassName("around")[0],
        document.getElementById("map"),
        document.getElementsByTagName("footer")[0]
    ];

    var offGold;
    var offEnd;
    var imageHeight;

    var offGoldEnd;
    var aroundSize = {h: 0, w: 0};





    var cs = [
        {s: 0.6, x: 360, y: 215, r: 5, a: 90, t: "Golfanlage Amper, Mühlbach", p: [360, 215 - 5, 360, 150]},
        {s: 0.7, x: 300, y: 580, r: 5, a: 180, t: "Olchinger See", p: [300 - 5, 580, 245, 580, 203, 543]},
        {s: 0.6, x: 1180, y: 215, r: 5, a: 90, t: "München", p: [1180, 215 - 5, 1180, 150]},
        //
        {s: 0.00, x: 915, y: 215, r: 8, a: 135, t: "", p: []},
        {s: 0.04, x: 915, y: 215, r: 11, a: 135, t: "", p: []},
        {s: 0.02, x: 915, y: 215, r: 14, a: 135, t: "", p: []},
        {s: 0.06, x: 915, y: 215, r: 17, a: 135, t: "myHotel", p: [915 - 5, 215 - 5, 874, 172, 874, 115]},
    ];

    for (var i = 0; i < cs.length; i++) {

        var p = cs[i].p;
        var d = 0;
        for (var j = 2; j < p.length; j += 2) {
            d += Math.sqrt(Math.pow(p[j] - p[j - 2], 2) + Math.pow(p[j + 1] - p[j - 1], 2));
        }
        cs[i].pl = d; // Path length
    }

    var dist = function(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };

    var infoState = 0;

    function drawInfo(P) {

        if (P >= 1) {
            P = 1;
        } else if (P <= 0) {
            P = 0;
        }

        infoState = P;

        var ctx = DOM.info.getContext("2d");

        var W = aroundSize.w;
        var H = aroundSize.h;

        ctx.clearRect(0, 0, W, H);

        var imW = 1500;
        var imH = 715;


        var Rx = W / imW;
        var Ry = H / imH;

        for (var i = 0; i < cs.length; i++) {

            var c = cs[i];
            var p = c.p;

            ctx.beginPath();
            ctx.arc(Rx * c.x, Ry * c.y, c.r, -c.a / 180 * Math.PI, -c.a / 180 * Math.PI + Math.PI * 2 * Math.max(0, Math.min(1, P * 4 - c.s)), false);
            ctx.stroke();

            var xP = Math.max(0, P - 0.2 - c.s) / (0.8 - c.s);

            ctx.fillStyle = "rgba(0,0,0," + Math.max(0, P - 0.9) / 0.1 + ")";
            ctx.font = "15pt Campton-Light";
            ctx.fillText(c.t, Rx * (p[p.length - 2] - ctx.measureText(c.t).width / 2), Ry * p[p.length - 1] - 12);




            var S = 0;
            for (var j = 2; j < p.length; j += 2) {

                var x1 = p[j - 2];
                var y1 = p[j - 1];
                var x2 = p[j];
                var y2 = p[j + 1];

                var seg = dist(x1, y1, x2, y2);
                var pct = Math.max(0, Math.min(1, (xP * c.pl - S) / seg));
                S += seg;

                ctx.beginPath();
                ctx.moveTo(Rx * x1, Ry * y1);
                ctx.lineTo(Rx * (x1 + pct * (x2 - x1)), Ry * (y1 + pct * (y2 - y1)));
                ctx.stroke();
            }

        }

    }

    window.onload = function() {

        initialize();
        window.onresize();

    };

    window.onresize = function() {

        positionStaySmart();
        positionCircles();

        var t = $(".imageBox", sections[4]);

        offGold = $(sections[2]).offset();
        offEnd = $(sections[3]).offset();
        imageHeight = $(sections[0]).height();
        offGoldEnd = $(sections[2]).offset().top + 76;
        aroundSize.h = t.height();
        aroundSize.w = t.width();

        DOM.info.width = aroundSize.w;
        DOM.info.height = aroundSize.h;

        drawInfo(infoState);

    };

    function goldenBox(p) {

        var w = 600; // maximale flugbreite, defin

        var d1 = 230; // äußere distance
        var d2 = 80; // innere distance

        domChange.c1 = circleCenter - Math.min(p * w, d1);
        domChange.c2 = circleCenter + Math.min(p * w, d2);
        domChange.c3 = circleCenter + Math.min(p * w, d1);

        domChange.t1 = Math.min(0.5 + p, 1);
        domChange.t2 = Math.min(0.5 + p, 1);
    }

    window.onscroll = function() {

        var s = $(window).scrollTop();

        // TEASER IMAGE
        var x = s / imageHeight;
        domChange.imgOffset = Math.min(Math.floor(x * 25), 70);

        // GOLDEN BOX
        if (s > offGold.top - 300) {
            goldenBox((s - offGold.top + 300) / 632);
        } else {
            goldenBox(0);
        }

        if (s >= offGoldEnd) {
            domChange.top = (s - offGoldEnd) / 100;
        } else {
            domChange.top = 0;
        }

        drawInfo((s - offEnd.top - 400) / 400);

    };

    var open = null;

    $(".toggleBtn").click(function(ev) {

        ev.preventDefault();

        var dest = $(this).data("dest");

        if (open !== null) {
            $("." + open).stop().slideUp(function() {
                open = null;
            });

            $('.toggleBtn').removeClass('active');
        }

        if (open === null || dest !== open) {
            $("." + dest).stop().slideDown(function() {
                $('html, body').animate({
                    scrollTop: $("." + dest).offset().top + $('window').height()
                }, 800);

                $('.toggleBtn').addClass('active');

                open = dest;
            });
        }

    });


})(this);