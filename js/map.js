/*-----------------------------------------------------------------------------------*/
/* Google Map */
/*-----------------------------------------------------------------------------------*/

function initialize() {

var styles = [
	{
		featureType: 'road',
		elementType: 'all',
		stylers: [
			{ hue: '#989078' },
			{ saturation: -80 },
			{ lightness: -6 },
			{ visibility: 'simplified' }
		]
	},{
		featureType: 'landscape',
		elementType: 'all',
		stylers: [
			{ hue: '#e2dac2' },
			{ saturation: 12 },
			{ lightness: -7 },
			{ visibility: 'simplified' }
		]
	},{
		featureType: 'water',
		elementType: 'all',
		stylers: [
			{ hue: '#bbb49b' },
			{ saturation: -80 },
			{ lightness: -11 },
			{ visibility: 'simplified' }
		]
	},{
		featureType: 'poi',
		elementType: 'all',
		stylers: [
			{ hue: '#d1cab1' },
			{ saturation: -40 },
			{ lightness: -3 },
			{ visibility: 'simplified' }
		]
	}
];

	myLatLng = new google.maps.LatLng(48.2293848, 11.3367019);

	var options = {
			mapTypeControlOptions: {
				mapTypeIds: [ 'Styled']
			},
			center: myLatLng,
			zoom: 12,
			mapTypeId: 'Styled',
			scrollwheel: false,
			draggable: true
	};

	var div 	= document.getElementById('map');
	var image 	= 'images/meta/marker.png';
	
	/*
	var icon = {
        url: '/images/meta/marker.svg',
        size: new google.maps.Size(128, 128),
        origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(32, 32)
	};
	*/
	var map 	= new google.maps.Map(div, options);

	var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
	map.mapTypes.set('Styled', styledMapType);

	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		icon: image
	});
}